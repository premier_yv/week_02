package ru.edu.empty;

import ru.edu.TextAnalyzer;
import ru.edu.TextStatistics;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Пустая реализация, только для компиляции
 */
public class EmptyTextAnalyzer implements TextAnalyzer {

    @Override
    public void analyze(String line) {

    }

    @Override
    public TextStatistics getStatistic() {
        return new TextStatistics();
    }
}
