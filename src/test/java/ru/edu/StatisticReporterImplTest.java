package ru.edu;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatisticReporterImplTest {

    public static final String RESULT_TXT = "./target/reporter_result.txt";

    @Test
   public void writeStats(){
        StatisticReporter reporter = new StatisticReporterImpl(RESULT_TXT);

        TextStatistics statistics = new TextStatistics();

        statistics.addCharsCount(100);
        statistics.addWordsCount(50);
        statistics.addCharsCountOnlyPunctuations(25);
        statistics.addCharsCountWithoutSpaces(10);
        
        reporter.report(statistics);

        List<String> lines = readFile(RESULT_TXT);
        assertEquals(4, lines.size());
        assertEquals("Слов: 50", lines.get(0));
        assertEquals("Символов: 100", lines.get(1));
        assertEquals("Не пробелов: 10", lines.get(2));
        assertEquals("Символов пунктуации: 25", lines.get(3));

    }

    private List<String> readFile(String resultTxt) {
        List<String> lines = new ArrayList<>();
        try(FileReader fr = new FileReader(new File(resultTxt));
            BufferedReader br = new BufferedReader(fr)){
            String line;
            while((line = br.readLine()) != null){
                    lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
