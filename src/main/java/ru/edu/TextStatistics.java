package ru.edu;

import java.util.Collections;
import java.util.List;

/**
 * Необходимо реализовать методы модификации
 * и доступа к хранимым приватным переменным.
 */
public class TextStatistics {

    /**
     * Всего слов.
     */
    private long wordsCount;

    /**
     * Всего символов.
     */
    private long charsCount;

    /**
     * Всего символов без пробелов.
     */
    private long charsCountWithoutSpaces;

    /**
     * Всего знаков препинания.
     */
    private long charsCountOnlyPunctuations;


    /**
     * Получение количества слов.
     *
     * @return значение
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Количество слов.
     * @param value
     */
    public void addWordsCount(final long value) {
        wordsCount += value;
    }

    /**
     * Получение количества символов.
     *
     * @return значение
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Количество символов.
     * @param value
     */
    public void addCharsCount(final long value) {
        charsCount += value;
    }

    /**
     * Получение количества слов без пробелов.
     *
     * @return значение
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Без пробелов.
     * @param value
     */

    public void addCharsCountWithoutSpaces(final long value) {
        charsCountWithoutSpaces += value;
    }

    /**
     * Получение количества знаков препинания.
     *
     * @return значение
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Знаков пунктуации.
     * @param value
     */
    public void addCharsCountOnlyPunctuations(final long value) {
        charsCountOnlyPunctuations += value;
    }

    /**
     * Задание со звездочкой.
     * Необходимо реализовать нахождение топ-10 слов.
     *
     * @return List из 10 популярных слов
     */
    public List<String> getTopWords() {
        return Collections.emptyList();
    }

    /**
     * Текстовое представление.
     *
     * @return текст
     */
    @Override
    public String toString() {
        return "TextStatistics{"
                + "wordsCount=" + wordsCount
                + ", charsCount=" + charsCount
                + ", charsCountWithoutSpaces=" + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations=" + charsCountOnlyPunctuations
                + '}';
    }
}
