package ru.edu;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SourceReaderImpl<line> implements SourceReader {

    /**
     * Файл для чтения.
     */

    private File file;

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     *
     * @param source
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException("source is null");
        }

        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException("source file is missing");
        }

    }

    /**
     * Метод для анализа источника.
     *
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {
        if (analyzer == null) {
            throw new IllegalArgumentException("analyzer is null");
        }

        try (
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr)
        ) {
            processFile(analyzer, br);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        //analyzer.analyze();

        return analyzer.getStatistic();
    }

    private void processFile(final TextAnalyzer analyzer,
                             final BufferedReader br
    ) throws IOException {
        String line;
        boolean firstLine = true;
        while ((line = br.readLine()) != null) {
             analyzer.analyze(!firstLine ? "\n" + line : line);
             firstLine = false;
        }

        br.readLine();
    }
}
